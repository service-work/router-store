# RouterStoreService

### Installation

```bash
yarn add @service-work/router-store

# or

npm install -s @service-work/router-store
```

### Configuation

By default, the `RouterStoreService` maintains a store of the router state that has the following interface

```ts
interface IDefaultRouterStore {
  id: number;
  url: string;
  urlAfterRedirects: string;
  state: {
    params: Params;
    queryParams: Params;
  };
}
```

If this is suitable for you, you can import and use `RouterStoreService` it as-is. However, you very likely will want to serialize additional / different information from Angular's router state (e.g. custom `data` from the route or information from secondary router outlets). In order to do this, you will need to provide a custom router state serializer function. To do this, re-provide the `SW_ROUTER_STORE_SERVICE_CONFIG` token.

For example:

```ts
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { Params } from "@angular/router";

import {
  IRouterStoreServiceConfig,
  SW_ROUTER_STORE_SERVICE_CONFIG,
} from "@service-work/router-store";

export interface IMyCustomRouterState {
  params: Params;
  queryParams: Params;
  myCustomBreadcrumbData: string[];
}

export const routerStoreConfig: IRouterStoreServiceConfig<IMyCustomRouterState> = {
  serializer: (snapshot: RouterStateSnapshot) => {
    let route = snapshot.root;
    let params: Params = {};
    let myCustomBreadcrumbData: string[] = [];

    while (route.firstChild) {
      route = route.firstChild;

      if (route.outlet === "primary") {
        params = {
          ...params,
          ...route.params,
        };

        if (route.data.myCustomBreadcrumbData) {
          myCustomBreadcrumbData.push(route.data.myCustomBreadcrumbData);
        }
      }
    }

    return {
      params,
      queryParams: snapshot.root.queryParams,
      myCustomBreadcrumbData,
    };
  },
};

@NgModule({
  imports: [BrowserModule, AppRoutingModule],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  providers: [
    { provide: SW_ROUTER_STORE_SERVICE_CONFIG, useValue: routerStoreConfig },
  ],
})
export class AppModule {}
```

Additionally, when now when making use of the `RouterStoreService` you'll need to provide the correct interface for your custom store.

For example,

```ts
@Injectable({ providedIn: "root" })
export class MyCustomService {
  constructor(private routerStore: RouterStoreService<IMyCustomRouterState>) {}
}
```

Always needing to provide this generic type argument can be annoying, so I recommend extending the `RouterStoreService` in your own app and turning it into a non-generic service.

For example,

```ts
import { Injectable } from "@angular/core";
import { IMyCustomRouterState } from "./app.module";
import { RouterStoreService } from "@service-work/router-store";

@Injectable({ providedIn: "root" })
export class MyRouterStore extends RouterStoreService<IMyCustomRouterState> {}
```

Then import your custom `MyRouterStore` into other components / services. For example,

```ts
import { Injectable } from "@angular/core";
import { MyRouterStore } from "./my-router-store";

@Injectable({ providedIn: "root" })
export class MyCustomService {
  constructor(private routerStore: MyRouterStore) {}
}
```
