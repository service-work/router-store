/*
 * Public API Surface of router-store
 */

export * from './lib/router-store.service';
export * from './lib/router-store.service.config';
