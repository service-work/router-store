import { Inject, Injectable, Optional } from '@angular/core';
import {
  Router,
  NavigationCancel,
  NavigationError,
  NavigationEnd,
  RoutesRecognized,
  ResolveEnd,
  Params,
  RouterStateSnapshot,
} from '@angular/router';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';
import {
  IRouterStoreServiceConfig,
  SW_ROUTER_STORE_SERVICE_CONFIG,
} from './router-store.service.config';

export interface IRouterStore<T> {
  id: number;
  url: string;
  urlAfterRedirects: string;
  state: T;
}

export interface IDefaultRouterStoreState {
  params: Params;
  queryParams: Params;
}

const DEFAULT_ROUTER_STORE_SERVICE_CONFIG: IRouterStoreServiceConfig<IDefaultRouterStoreState> = {
  serializer: (snapshot: RouterStateSnapshot) => {
    let route = snapshot.root;
    let params: Params = {};

    while (route.firstChild) {
      route = route.firstChild;

      if (route.outlet === 'primary') {
        params = {
          ...params,
          ...route.params,
        };
      }
    }

    return { params, queryParams: snapshot.root.queryParams };
  },
  isEqual: (a, b) => a === b,
};

@Injectable({
  providedIn: 'root',
})
export class RouterStoreService<T = IDefaultRouterStoreState> {
  protected _currentStore: BehaviorSubject<IRouterStore<T>>;
  protected _nextStore: BehaviorSubject<IRouterStore<T>>;

  protected config: Required<IRouterStoreServiceConfig<T>>;

  constructor(
    private router: Router,
    @Optional()
    @Inject(SW_ROUTER_STORE_SERVICE_CONFIG)
    config: IRouterStoreServiceConfig<T> | null
  ) {
    this.config = {
      ...DEFAULT_ROUTER_STORE_SERVICE_CONFIG,
      ...config,
    } as Required<IRouterStoreServiceConfig<T>>;

    const initialState: IRouterStore<T> = {
      id: 0,
      url: this.router.url,
      urlAfterRedirects: this.router.url,
      state: this.config.serializer(this.router.routerState.snapshot),
    };

    this._currentStore = new BehaviorSubject(initialState);
    this._nextStore = new BehaviorSubject(initialState);

    this.router.events.subscribe((event) => {
      if (event instanceof RoutesRecognized) {
        const state = this.config.serializer(event.state);
        this._nextStore.next({
          id: event.id,
          url: event.url,
          urlAfterRedirects: event.urlAfterRedirects,
          state,
        });
      } else if (
        event instanceof NavigationCancel ||
        event instanceof NavigationError
      ) {
        this._nextStore.next(this._currentStore.value);
      } else if (event instanceof ResolveEnd) {
        // resolvers can put data in the routes `data` property
        // so we need to reserialize the event state to make this
        // data available
        const state = this.config.serializer(event.state);
        this._nextStore.next({
          id: event.id,
          url: event.url,
          urlAfterRedirects: event.urlAfterRedirects,
          state,
        });
      } else if (event instanceof NavigationEnd) {
        this._currentStore.next(this._nextStore.value);
      }
    });
  }

  get<S>(fn: (current: IRouterStore<T>, next: IRouterStore<T>) => S): S {
    return fn(this._currentStore.value, this._nextStore.value);
  }

  get$<S>(
    fn: (current: IRouterStore<T>, next: IRouterStore<T>) => S
  ): Observable<S> {
    return combineLatest([this._currentStore, this._nextStore]).pipe(
      map(([current, next]) => fn(current, next)),
      distinctUntilChanged<S>(this.config.isEqual)
    );
  }
}
