import { InjectionToken } from '@angular/core';
import { Params, RouterStateSnapshot } from '@angular/router';

export interface IRouterStoreServiceConfig<T> {
  serializer: (snapshot: RouterStateSnapshot) => T;
  isEqual?: (a: unknown, b: unknown) => boolean;
}

export const SW_ROUTER_STORE_SERVICE_CONFIG = new InjectionToken(
  'SW_ROUTER_STORE_SERVICE_CONFIG'
);
